FROM      mcr.microsoft.com/dotnet/core/sdk:3.1
WORKDIR   /app
COPY      *.csproj ./
RUN        dotnet restore
COPY      ../engine/examples 
RUN        dotnet publish -c Release -o out
FROM       mcr.microsoft.com/dotnet/core/sdk:3.1
WORKDIR    /app
ENTRYPOINT  ["dotnet", "webapplication.dll"]